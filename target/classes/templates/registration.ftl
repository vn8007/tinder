<html lang="en">
<head>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" href="/assets/css/style.css">
</head>

<body class="text-center">
<form action="/registration" method="post" class="form-signin">
    <img class="mb-4" src="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/87/fc/dd/87fcdd00-b061-50d5-48ce-b945c1cbd47a/AppIcon-1x_U007emarketing-0-7-0-85-220.png/1200x630wa.png" alt=""  height="72">
    <h1 class="h3 mb-3 font-weight-normal">Registration</h1>
    <label for="login" class="sr-only">Login</label>
        <input type="text" name="login" placeholder="enter login" class="form-control" required autofocus/>
    <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" placeholder="enter password" class="form-control" required/>
    <label for="name" class="sr-only">Name</label>
        <input type="text" name="name" placeholder="enter your name" class="form-control" required/>
    <label for="surname" class="sr-only">Surname</label>
        <input type="text" name="surname" placeholder="enter your surname" class="form-control" required/>
    <label for="photoLink" class="sr-only">Photo Link</label>
        <input type="text" name="photoLink" placeholder="enter your photo(link)" class="form-control" required/>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Ok</button>
    <p>or
        <a href="/login">login</a>
</form>
</body>
</html>
