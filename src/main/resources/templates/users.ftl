<html lang="en">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link rel="stylesheet" href="../assets/css/style.css">

<body>
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2">
            <div class="panel panel-default user_panel">
                <div class="panel-heading display-block">
                    <div>
                    <h3 class="panel-title">User List</h3>
                    </div>
                    <div>
                    <a href="/logout" class="text-decor">
                        <i class="fa fa-times hover text-center pt-1"></i>
                    </a>
                    </div>
                </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <#if users??>
                            <table class="table-users table" border="0">
                                <tbody>
                                    <#list users as user>
                                        <tr>
                                            <td width="10">
                                                <div class="avatar-img">
                                                    <a href="/user?id=${user.id}">
                                                    <img class="img-circle" src="${user.photoLink}" />
                                                    </a>
                                                </div>
                                            </td>
                                            <td class="align-middle">
                                                ${user.name} ${user.surname}
                                            </td>
                                            <td class="align-middle">
                                                Builder Sales Agent
                                            </td>
                                            <td  class="align-middle">
                                                Last Login:  6/10/2017<br><small class="text-muted">5 days ago</small>
                                            </td>
                                        </tr>
                                    </#list>
                                </tbody>
                            </table>
                            </#if>
                    </div>
                </div>
            </div>
            </div>
    </div>
</div>
</body>
</html>
