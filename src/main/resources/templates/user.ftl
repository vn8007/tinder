<html lang="en">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link rel="stylesheet" href="../assets/css/style.css">
<body style="background-color: #f5f5f5;">

<div class="col-4 offset-4">
    <#if user??>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 text-center">
                    <img src="${user.photoLink}" alt="" class="mx-auto rounded-circle img-fluid">
                    <h3 class="mb-0 text-truncated">${user.name} ${user.surname}</h3>
                    <br>
                </div>
                <div class="col-12 col-lg-6">
                    <a href="/users" class="text-decor">
                    <button type="button" class="btn btn-outline-danger btn-block"><span class="fa fa-times"></span> Dislike</button>
                    </a>
                </div>
                <div class="col-12 col-lg-6">
                    <a href="/messages?id=${user.id}" class="text-decor">
                    <button class="btn btn-outline-success btn-block"><span class="fa fa-heart"></span> Like</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    </#if>


</div>

</body>
</html>