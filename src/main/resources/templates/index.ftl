<html lang="en">

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet">

<link rel="stylesheet" href="../assets/css/style.css">
<body class="text-center">
<form action="/users" method="post" class="form-signin">
    <img class="mb-4" src="https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/87/fc/dd/87fcdd00-b061-50d5-48ce-b945c1cbd47a/AppIcon-1x_U007emarketing-0-7-0-85-220.png/1200x630wa.png" alt=""  height="72">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="login" class="sr-only">Login</label>
    <input type="text" id="login" name="login" class="form-control" placeholder="Login" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <p>for new registration:
            <a href="/registration">click here</a>
    <p class="mt-5 mb-3 text-muted">&copy; Tinder 2018</p>
</form>

</body>
</html>