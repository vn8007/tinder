package com.tinder.dao;

public class User {
    private Long id;
    private String name;
    private String surname;
    private String photoLink;
    private String login;
    private String password;

    public User(String name, String surname, String photoLink, String login, String password) {
        this.name = name;
        this.surname = surname;
        this.photoLink = photoLink;
        this.login = login;
        this.password = password;
    }

    public User(Long id, String name, String surname, String photoLink, String login, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.photoLink = photoLink;
        this.login = login;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", photoLink='" + photoLink + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
