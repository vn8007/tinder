package com.tinder.dao;

import org.postgresql.ds.PGPoolingDataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserJdbcDao implements UserDao{
    private PGPoolingDataSource source;

    public UserJdbcDao(){
        source = new PGPoolingDataSource();
        source.setServerName("ec2-34-249-161-200.eu-west-1.compute.amazonaws.com");
        source.setDatabaseName("d54jtnl6v7olib");
        source.setUser("zknshvagfbhinj");
        source.setPassword("41afa18e22fcca7d2ad0f724143db7f3930bb8656f9764cd01d8b2f7421eb821");
//        source.setServerName("ec2-54-76-43-89.eu-west-1.compute.amazonaws.com");
//        source.setDatabaseName("d2ip3n52okiao");
//        source.setUser("ddmnrxjlczvwfw");
//        source.setPassword("a80a93f982ed295765eec95806f702d9a4ca650204c3a16c61c0ec3738a06e5f");
        source.setMaxConnections(10);
    }

    @Override
    public boolean create(User user) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO users.users(name, surname, photolink, login, password) VALUES (?, ?, ?, ?, ?)");
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getPhotoLink());
            preparedStatement.setString(4, user.getLogin());
            preparedStatement.setString(5, user.getPassword());


            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;

    }

    @Override
    public User read(Long userId) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users.users WHERE id = ?");
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long idUser = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String photoLink = resultSet.getString("photoLink");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                return new User(idUser, name, surname, photoLink, login, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;

    }

    @Override
    public void update(User user) {

    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<User> findAll(long myId) {
        List<User> users = new ArrayList<>();
        Connection connection = null;
        try {
            connection = source.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM users.users");
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String photoLink = resultSet.getString("photoLink");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                users.add(new User(id, name, surname, photoLink, login, password));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        users.removeIf(c -> c.getId() == myId);
        return users;

    }

    @Override
    public User findByLoginPass(String loginUser, String passwordUser) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM users.users WHERE login=? AND password=?");
            preparedStatement.setString(1, loginUser);
            preparedStatement.setString(2, passwordUser);

            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String surname = resultSet.getString("surname");
                String photoLink = resultSet.getString("photoLink");
                String login = resultSet.getString("login");
                String password = resultSet.getString("password");
                return new User(id, name, surname, photoLink, login, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;

    }
}
