package com.tinder.dao;

import org.postgresql.ds.PGPoolingDataSource;

import java.sql.*;
import java.util.*;


public class MessageJdbcDao implements MessageDao{
    private PGPoolingDataSource source;

    public MessageJdbcDao(){
        source = new PGPoolingDataSource();
        source.setServerName("ec2-34-249-161-200.eu-west-1.compute.amazonaws.com");
        source.setDatabaseName("d54jtnl6v7olib");
        source.setUser("zknshvagfbhinj");
        source.setPassword("41afa18e22fcca7d2ad0f724143db7f3930bb8656f9764cd01d8b2f7421eb821");
//        source.setServerName("ec2-54-76-43-89.eu-west-1.compute.amazonaws.com");
//        source.setDatabaseName("d2ip3n52okiao");
//        source.setUser("ddmnrxjlczvwfw");
//        source.setPassword("a80a93f982ed295765eec95806f702d9a4ca650204c3a16c61c0ec3738a06e5f");
        source.setMaxConnections(10);
    }


    @Override
    public boolean create(Message message) {
        Connection connection = null;
        try {
            connection = source.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO messages.messages(fromId, toId, msg, msgTime) VALUES (?, ?, ?, ?)");
            preparedStatement.setLong(1, message.getFromId());
            preparedStatement.setLong(2, message.getToId());
            preparedStatement.setString(3, message.getMsg());
            preparedStatement.setLong(4, message.getMsgTime());


            int executionResult = preparedStatement.executeUpdate();
            connection.commit();

            return executionResult > 0;

        } catch (SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public List<Message> createChat(long fromIdUser, long toIdUser) {
        List<Message> messages = new ArrayList<>();
        Connection connection = null;
        try {
            connection = source.getConnection();
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM messages.messages WHERE fromid = ? AND toid = ?");
            preparedStatement1.setLong(1, fromIdUser);
            preparedStatement1.setLong(2, toIdUser);

            ResultSet resultSet1 = preparedStatement1.executeQuery();
            while(resultSet1.next()) {
                long fromId = resultSet1.getLong("fromid");
                long toId = resultSet1.getLong("toid");
                String msg = resultSet1.getString("msg");
                long msgTime = resultSet1.getLong("msgtime");
                String classCss = "right";
                messages.add(new Message(fromId, toId, msg, msgTime, classCss));
            }
            PreparedStatement preparedStatement2 = connection.prepareStatement("SELECT * FROM messages.messages WHERE fromid = ? AND toid = ?");
            preparedStatement2.setLong(1, toIdUser);
            preparedStatement2.setLong(2, fromIdUser);


            ResultSet resultSet2 = preparedStatement2.executeQuery();
            while(resultSet2.next()) {
                long fromId = resultSet2.getLong("fromid");
                long toId = resultSet2.getLong("toid");
                String msg = resultSet2.getString("msg");
                long msgTime = resultSet2.getLong("msgtime");
                String classCss = "left";
                messages.add(new Message(fromId, toId, msg, msgTime, classCss));
            }
            messages.sort(new Comparator<Message>() {
                @Override
                public int compare(Message o1, Message o2) {
                    if(o1.getMsgTime() == o2.getMsgTime()) return 0;
                    else if(o1.getMsgTime() > o2.getMsgTime()) return 1;
                    else return -1;
                }
            });


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return messages;

    }
}
