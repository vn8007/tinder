package com.tinder.dao;

import java.util.List;

public interface MessageDao {

    boolean create (Message message);
    List<Message> createChat(long fromId, long toId);
}
