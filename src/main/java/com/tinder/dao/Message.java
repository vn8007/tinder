package com.tinder.dao;

public class Message implements Comparable<Message> {
    private Long id;
    private long fromId;
    private long toId;
    private String msg;
    private long msgTime;
    private String classCss;

    public String getClassCss() {
        return classCss;
    }

    public void setClassCss(String classCss) {
        this.classCss = classCss;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFromId() {
        return fromId;
    }

    public void setFromId(long fromId) {
        this.fromId = fromId;
    }

    public long getToId() {
        return toId;
    }

    public void setToId(long toId) {
        this.toId = toId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(long msgTime) {
        this.msgTime = msgTime;
    }

    public Message(long fromId, long toId, String msg, long msgTime, String classCss) {
        this.fromId = fromId;
        this.toId = toId;
        this.msg = msg;
        this.msgTime = msgTime;
        this.classCss = classCss;
    }

    public Message(Long id, long fromId, long toId, String msg, long msgTime) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.msg = msg;
        this.msgTime = msgTime;
    }

    @Override
    public int compareTo(Message o) {
        return (int)(this.getMsgTime() - o.getMsgTime());
    }
}
