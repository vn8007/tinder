package com.tinder;

import com.tinder.controller.*;
import com.tinder.dao.MessageDao;
import com.tinder.dao.MessageJdbcDao;
import com.tinder.dao.UserDao;
import com.tinder.dao.UserJdbcDao;
import com.tinder.service.MessageService;
import com.tinder.service.MessageServiceDefault;
import com.tinder.service.UserService;
import com.tinder.service.UserServiceDefault;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class JettyRun {
    public static void main(String[] args) throws Exception{
        String webPort = System.getenv("PORT");
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }
        Integer port = Integer.parseInt(webPort);
        System.out.println("PORT: " + port );

        Server server = new Server(port);
        ServletContextHandler handler = new ServletContextHandler();
        final UserDao userDao = new UserJdbcDao();
        UserService userService = new UserServiceDefault(userDao);
        TemplateEngine templateEngine = new TemplateEngine();
        final MessageDao messageDao = new MessageJdbcDao();
        MessageService messageService = new MessageServiceDefault(messageDao);


        handler.addServlet(new ServletHolder(new LoginServlet(userService, templateEngine)), "/");
        handler.addServlet(new ServletHolder(new LogoutServlet(templateEngine)), "/logout");
        handler.addServlet(new ServletHolder(new FileServlet()), "/assets/*");
        handler.addFilter(new FilterHolder(new LoginFilter(templateEngine, userService)), "/*", EnumSet.of(DispatcherType.REQUEST));
        handler.addServlet(new ServletHolder(new UsersServlet(userService, templateEngine)), "/users");
        handler.addServlet(new ServletHolder(new RegistrationServlet(userService, templateEngine)),"/registration");
        handler.addServlet(new ServletHolder(new UserServlet(templateEngine, userService)), "/user/*");
        handler.addServlet(new ServletHolder(new MessagesServlet(messageService, userService, templateEngine)), "/messages");


        server.setHandler(handler);
        server.start();
        server.join();
    }
}
