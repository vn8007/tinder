package com.tinder.controller;

import com.tinder.dao.Message;
import com.tinder.service.CookieUtil;
import com.tinder.service.MessageService;
import com.tinder.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import static com.tinder.controller.LoginFilter.USER_PARAM_ID;

public class MessagesServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private MessageService messageService;
    private UserService userService;

    public MessagesServlet(MessageService messageService, UserService userService, TemplateEngine templateEngine){
        this.templateEngine = templateEngine;
        this.messageService = messageService;
        this.userService = userService;
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        long fromId = Long.parseLong(cookieByName.get().getValue());

        long toId = Long.parseLong(req.getParameter("id"));
        HashMap<String, Object> data = new HashMap<>(1);
        data.put("messages", messageService.createChat(fromId, toId));
        data.put("user", userService.read(toId));
        templateEngine.render("messages.ftl", data, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        Date currentDate = new Date();
        long fromId = Long.parseLong(cookieByName.get().getValue());
        long toId = Long.parseLong(req.getParameter("id"));
        String msg = req.getParameter("msg");
        long msgTime = currentDate.getTime();
        Message message = new Message(null, fromId, toId, msg, msgTime);
        messageService.create(message);
        resp.sendRedirect("/messages?id="+req.getParameter("id"));
    }
}
