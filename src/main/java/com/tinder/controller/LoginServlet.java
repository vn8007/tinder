package com.tinder.controller;

import com.tinder.service.CookieUtil;
import com.tinder.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.tinder.controller.LoginFilter.USER_PARAM_ID;

public class LoginServlet extends HttpServlet {
    private UserService userService;
    private TemplateEngine templateEngine;

    public LoginServlet(UserService userService, TemplateEngine templateEngine){
        this.userService = userService;
        this.templateEngine = templateEngine;
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        if(cookieByName.isPresent()){resp.sendRedirect("/users");}
        else {
            templateEngine.render("index.ftl", resp);
        };
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        if(cookieByName.isPresent()){resp.sendRedirect("/users");}
        else {
            templateEngine.render("index.ftl", resp);
        };
    }
}
