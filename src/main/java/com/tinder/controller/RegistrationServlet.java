package com.tinder.controller;

import com.tinder.dao.User;
import com.tinder.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RegistrationServlet extends HttpServlet {
    private UserService userService;
    private TemplateEngine templateEngine;

    public RegistrationServlet(){};

    public RegistrationServlet(UserService userService, TemplateEngine templateEngine){
        this.templateEngine = templateEngine;
        this.userService = userService;
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> params = new HashMap<>();
        templateEngine.render("registration.ftl", params, resp );
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String photoLink = req.getParameter("photoLink");
        User user = new User(null, name, surname, photoLink, login, password);
        userService.create(user);
        resp.sendRedirect("/users");
    }



}
