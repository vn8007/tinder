package com.tinder.controller;

import com.tinder.dao.User;
import com.tinder.service.CookieUtil;
import com.tinder.service.UserService;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.Set;

public class LoginFilter implements Filter {
    public static final String USER_PARAM_ID = "id";
    private TemplateEngine templateEngine;
    private UserService userService;

    public LoginFilter(TemplateEngine templateEngine, UserService userService) {
        this.templateEngine = templateEngine;
        this.userService = userService;
    }

    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp,
                         FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        Set<String> allowedUrls = Set.of("/registration", "/assets");
        if (allowedUrls.contains(request.getServletPath()) ) {
            chain.doFilter(request, response);
        }
        Optional<Cookie> optionalCookie = CookieUtil.getCookieByName(request, USER_PARAM_ID);// "id"
        if (optionalCookie.isPresent()) {
            chain.doFilter(request, response);
        } else {
            String login = request.getParameter("login");
            String password = request.getParameter("password");

            User user = userService.findByLoginPass(login, password);
            if (user != null) {
                response.addCookie(new Cookie(USER_PARAM_ID, String.valueOf(user.getId())));
                chain.doFilter(request, response);
            }
            templateEngine.render("index.ftl", new HashMap<>(), response);
        }
    }



}
