package com.tinder.controller;

import com.tinder.dao.User;
import com.tinder.service.CookieUtil;
import com.tinder.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;

import static com.tinder.controller.LoginFilter.USER_PARAM_ID;

public class UsersServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private UserService userService;
    public UsersServlet(UserService userService, TemplateEngine templateEngine){
        this.templateEngine = templateEngine;
        this.userService = userService;
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        long myId;
        if (cookieByName.isPresent()) {
            myId = Long.parseLong(cookieByName.get().getValue());
        } else {
            String login = req.getParameter("login");
            String password = req.getParameter("password");

            User user = userService.findByLoginPass(login, password);
            myId = user.getId();
        }
        ;
        HashMap<String, Object> data = new HashMap<>(1);
        data.put("users", userService.findAll(myId));
        templateEngine.render("users.ftl", data, resp);
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<Cookie> cookieByName = CookieUtil.getCookieByName(req, USER_PARAM_ID);
        long myId = Long.parseLong(cookieByName.get().getValue());
        HashMap<String, Object> data = new HashMap<>(1);
        data.put("users", userService.findAll(myId));
        templateEngine.render("users.ftl", data, resp);
    }
}
