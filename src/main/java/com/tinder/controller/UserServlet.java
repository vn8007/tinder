package com.tinder.controller;

import com.tinder.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class UserServlet extends HttpServlet {
    private TemplateEngine templateEngine;
    private UserService userService;

    public UserServlet(TemplateEngine templateEngine, UserService userService){
        this.templateEngine = templateEngine;
        this.userService = userService;
    }
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));
        HashMap<String, Object> data = new HashMap<>(1);
        data.put("user", userService.read(id));
        templateEngine.render("user.ftl", data, resp);
    }
}