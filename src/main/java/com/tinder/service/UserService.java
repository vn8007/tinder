package com.tinder.service;

import com.tinder.dao.User;

import java.io.InputStream;
import java.util.List;

public interface UserService {
    boolean create(User user);
    User read(Long id);
    void update(User user);
    boolean delete(long id);
    List<User> findAll();

    List<User> findAll(long myId);

    User findByLoginPass(String login, String password);
}
