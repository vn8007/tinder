package com.tinder.service;

import com.tinder.dao.Message;
import com.tinder.dao.MessageDao;

import java.util.List;

public class MessageServiceDefault implements MessageService{
    private MessageDao messageDao;
    public MessageServiceDefault(MessageDao messageDao){
        this.messageDao = messageDao;
    }
    @Override
    public boolean create(Message message) {
        return messageDao.create(message);
    }

    @Override
    public List<Message> createChat(long fromId, long toId) {
        return messageDao.createChat(fromId, toId);
    }
}
