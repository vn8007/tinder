package com.tinder.service;

import com.tinder.dao.Message;

import java.util.List;

public interface MessageService {
    boolean create(Message message);
    List<Message> createChat(long fromId, long toId);
}
