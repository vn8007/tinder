package com.tinder.service;

import com.tinder.dao.User;
import com.tinder.dao.UserDao;

import java.util.List;

public class UserServiceDefault implements UserService{
    private UserDao userDao;

    public UserServiceDefault(UserDao userDao){
        this.userDao = userDao;
    }

    @Override
    public boolean create(User user) {
        return userDao.create(user);
    }

    @Override
    public User read(Long id) {
        return userDao.read(id);
    }

    @Override
    public void update(User user) {

    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public List<User> findAll(long myId) {
        return userDao.findAll(myId);
    }

    @Override
    public User findByLoginPass(String login, String password) {
        return userDao.findByLoginPass(login, password);
    }
}
